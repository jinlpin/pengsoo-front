window.onload = function() {

	var chart = new CanvasJS.Chart("chartContainer", {
		exportEnabled: true,
		animationEnabled: true,
		title: {
			text: "성별"
		},
		data: [{
			type: "pie",
			startAngle: 25,
			toolTipContent: "<b>{label}</b>: {y}명",
			showInLegend: "true",
			legendText: "{label}",
			indexLabelFontSize: 16,
			indexLabel: "{label} - {y}명",
			dataPoints: [
				{ y: 3320, label: "남자" },
				{ y: 2440, label: "여자" }
			]
		}]
	});
	var chart1 = new CanvasJS.Chart("chartContainer1", {
		exportEnabled: true,
		animationEnabled: true,
		title: {
			text: "나이"
		},
		data: [{
			type: "pie",
			startAngle: 75,
			toolTipContent: "<b>{label}</b>: {y}명",
			showInLegend: "true",
			legendText: "{label}",
			indexLabelFontSize: 16,
			indexLabel: "{label} - {y}명",
			dataPoints: [
				{ y: 12, label: "20대" },
				{ y: 42, label: "30대" },
				{ y: 32, label: "40대" },
				{ y: 42, label: "50대 이상" }			]
		}]
	});
	
	var chart2 = new CanvasJS.Chart("chartContainer2", {
		exportEnabled: true,
		animationEnabled: true,
		title: {
			text: "연봉별 분류"
		},
		data: [{
			type: "pie",
			startAngle: 25,
			toolTipContent: "<b>{label}</b>: {y}명",
			showInLegend: "true",
			legendText: "{label}",
			indexLabelFontSize: 16,
			indexLabel: "{label} - {y}명",
			dataPoints: [
				{ y: 332, label: "1천만원 이상" },
				{ y: 442, label: "3천만원 이상" },
				{ y: 123, label: "5천만원 이상" },
				{ y: 256, label: "7천만원 이상" },
				{ y: 23, label: "1억 이상" }			]
		}]
	});

	var chart3 = new CanvasJS.Chart("chartContainer3", {
		exportEnabled: true,
		animationEnabled: true,
		title: {
			text: "신용등급별 분류"
		},
		data: [{
			type: "pie",
			startAngle: 25,
			toolTipContent: "<b>{label}</b>: {y}명",
			showInLegend: "true",
			legendText: "{label}",
			indexLabelFontSize: 16,
			indexLabel: "{label} - {y}명",
			dataPoints: [
				{ y: 443, label: "1등급" },
				{ y: 223, label: "2등급" },
				{ y: 553, label: "3등급" },
				{ y: 213, label: "4등급" },
				{ y: 153, label: "5등급" }			]
		}]
	});

	var chart4 = new CanvasJS.Chart("chartContainer4", {
		exportEnabled: true,
		animationEnabled: true,
		title: {
			text: "펀드보유계좌별 분류"
		},
		data: [{
			type: "pie",
			startAngle: 25,
			toolTipContent: "<b>{label}</b>: {y}명",
			showInLegend: "true",
			legendText: "{label}",
			indexLabelFontSize: 16,
			indexLabel: "{label} - {y}명",
			dataPoints: [
				{ y: 442, label: "0좌" },
				{ y: 11, label: "1좌" },
				{ y: 2, label: "2좌" },
				{ y: 44, label: "3좌" },
				{ y: 55, label: "4좌" },
				{ y: 66, label: "5좌 이상" }			]
		}]
	});

	var chart5 = new CanvasJS.Chart("chartContainer5", {
		exportEnabled: true,
		animationEnabled: true,
		title: {
			text: "대출보유계좌별 분류"
		},
		data: [{
			type: "pie",
			startAngle: 25,
			toolTipContent: "<b>{label}</b>: {y}명",
			showInLegend: "true",
			legendText: "{label}",
			indexLabelFontSize: 16,
			indexLabel: "{label} - {y}명",
			dataPoints: [
				{ y: 22, label: "0좌" },
				{ y: 33, label: "1좌" },
				{ y: 455, label: "2좌" },
				{ y: 11, label: "3좌" },
				{ y: 32, label: "4좌" },
				{ y: 32, label: "5좌" }		 ]
		}]
	});

	chart.render();
	chart1.render();
	chart2.render();
	chart3.render();
	chart4.render();
	chart5.render();
	}
	
