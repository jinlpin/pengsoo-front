//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const util = require(__BASEDIR+'/util');
const axios = require('axios');
//-----------

//---- 기본 library 셋팅
const __SPRING_SERVICE = 'http://recommendation-service.169.56.170.165.nip.io';

const router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(cookieParser());
//--------------

/*
 * Spring Boot 서비스(GET) 호출
 */
router.get("/spring", (req, res) => {
	util.log("Spring Boot 서비스(GET) 호출");

	axios.get(__SPRING_SERVICE+"/serviceGet/T.T.T")
	.then((ret) => {
		if(ret.status == 200) {
			res.json({
				resText: ret.data.inputData
			});
		} else {
			res.json({
				resText: "!200"
			});
		}
	})
	.catch((error) => {
		console.error(error);
		res.json({
			resText: error
		});
	});
});

/*
 * Spring Boot 서비스(POST) 호출
 */
router.post("/springPost", (req, res) => {
	util.log("Spring Boot 서비스(POST) 호출");
	util.log("입력데이터: " + req.query);
//	var inData = req.query;
	let body = req.body;

//	axios.post(__SPRING_SERVICE+"/questionAnswer", inData)
	axios.post(__SPRING_SERVICE+"/questionAnswer", 
	body)	
	.then((ret) => {
		if(ret.status == 200) {			
			console.log(ret.data.inputData);
			res.render("views/page3", {postData: body});
//			res.json({
//				resText: JSON.stringify(ret.data.inputData) + " <nodejs에서 추가>"
//			});			
		} else {
			res.json({
				resText: "!200"
			});
		}
	})
	.catch((error) => {
		console.error(error);
		res.json({
			resText: error
		});
	});	
	
});

/*
 * GET 서비스 예제
 */
router.get("/sample", (req, res) => {
	util.log("Get Service");

	res.json({
		resText: "Server Data!!!"
	});
});

/*
 * POST 서비스 예제
 */
router.post("/echo", (req, res) => {
	util.log("Post Service");

	var inData = req.query;
	res.json({
		resText: inData.txtPostRes + " @.@"
	});
});



module.exports = router;