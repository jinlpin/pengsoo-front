//--- import libraries
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cookieParser = require('cookie-parser');
//----------

var session = require('express-session');
//--- global constants & 환경변수
global.__BASEDIR = __dirname + '/';
global.__userId = '';
global.__userName = '';
global.__gender = '';
global.__age = '';
global.__wageTransfer = '';
global.__telPymntDirDbt = '';
global.__aptExpenDirDbt = '';
global.__overThreeYearTrans = '';
global.__marketingAgree = '';
global.__usingHanacard = '';
const port = (process.env.PORT || 8090);
//--------

//---- 기본 library 셋팅
const app = express();
app.use(express.static(path.join(__BASEDIR, '/public')));		//static resource 폴더 
app.use(bodyParser.urlencoded({extended:false}));				//include request 객체 parser
//app.use(express.bodyParser());
app.use(cookieParser());										//include cookie parser
app.use(session({
	 secret: '@#@$MYSIGN#@$#$',
	 resave: false,
	 saveUninitialized: true
	}));
//-----------

//--- ejs(Embed JS) 환경 셋팅
app.set('view engine','ejs');							//ui page rendering 시 ejs 사용
app.set('views', path.join(__BASEDIR, '/templates'));	//ui rendering시 사용할 ejs파일 위치 지정
//-------------

//--- include 개발 모듈
app.use(require(path.join(__BASEDIR, "/routes/login.js")));
app.use(require(path.join(__BASEDIR, "/routes/recommend.js")));		//include 상품추천처리
//--------

//----- start web server 
app.listen(port, () => {
	console.log('Listen: ' + port);
});
//----------------
