package com.klab;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SuppressWarnings({ "rawtypes", "unchecked" })
public class SampleController {

	@GetMapping("/serviceGet/{parm}")
	public Map processGet(@PathVariable String parm) {
		Map rs = new HashMap();
		
		rs.put("inputData", parm);
		
		return rs;
	}
	
	@PostMapping("/servicePost")
	public Map processPost(@RequestBody Map parm) {
		
		System.out.println("#### " + parm);
		
		Map rs = new HashMap();
		
		rs.put("inputData", parm);
		
		return rs;
	}
}
